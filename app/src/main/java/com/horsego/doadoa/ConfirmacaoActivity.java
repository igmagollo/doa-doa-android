package com.horsego.doadoa;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
// Nome nada intuitivo, devemos mudar
// TODO: Mudar este nome
public class ConfirmacaoActivity extends AppCompatActivity{
    // nome_inst recebida da activity anterior
    private String nome_inst;
    // id_inst recebida da activity anterior
    private int id_inst;
    // Edittext para a pessoa colocar o valor que ela deseja
    private EditText valor;
    // Boto que realiza a ação de gerar um boleto
    private Button bt;
    // TODO: Realizar a aquisição dinâmica de tokens
    private String token;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Seleciona o visual criado
        setContentView(R.layout.valor_e_doar);
        // Pega as informações extras enviadas pela activity anterior
        Bundle extras = getIntent().getExtras();
        if(extras==null)
        {
            // Se não existir nada
            nome_inst = null;
            id_inst = 0;
            // Voltar para a activity anterior
            finish();
        }
        else
        {
            // Se não carregar as informações mandadas
            nome_inst = extras.getString(getString(R.string.json_inst_nome));
            id_inst = extras.getInt(getString(R.string.json_inst_id));
        }
        // Faz o link com o visual
        valor = findViewById(R.id.valor);
        bt = findViewById(R.id.botao_doar);
        // Checa a versãi do android para alterar o título da activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setTitle(nome_inst);
        }
    }
    // Ação do botão doar
    public void doar(View view)
    {
        // Desabilita o botão, para a pessoa não clicar dnv
        bt.setEnabled(false);
        bt.setClickable(false);
        // Executa a tarefa de doação
        new GetDataTask().execute();
    }

    // Indica para o linter ignorar o problema "StaticFieldLeak"
    @SuppressLint("StaticFieldLeak")
    // Podemos modularizar isso ver iss59
    private class GetDataTask extends AsyncTask<Void, Void, String> {
        // Cliente que realiza aquisição http
        private OkHttpClient client;
        // Valores a serem usados
        private String url;
        private String valor_num;
        // ProgressDialog
        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            // Cria a telinha de carregando
            dialog = new ProgressDialog(ConfirmacaoActivity.this);
            client =  new OkHttpClient();
            // Url usada
            url = getString(R.string.server_name)+":"+getString(R.string.server_port)+getString(R.string.api_boleto_generate);
            // Valor que o caboclo digitou
            valor_num = valor.getText().toString();
            // Mensagem fofa
            dialog.setMessage(getString(R.string.info_loading_boleto));
            // Mostra tela de carregando
            dialog.show();

        }
        @Override
        protected String doInBackground(Void... params)
        {
            url=url+"?token="+token+"&valor="+valor_num+"&dest="+id_inst;
            Request request = new Request.Builder()
                    .header(getString(R.string.utils_content_type), getString(R.string.utils_application_form_encoded))
                    .url(url)
                    .get()
                    .build();
            // Cria a resposta
            Response response;
            try {
                // Faz o call, e o resultado é guardado em response
                response = client.newCall(request).execute();
                // Envia o resulta pro preExecute
                return response.body().string();
            } catch (Exception e) {
                // Escreve erros
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // Fecha o modal de carregamento se estiver sendo exibido
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(s!=null && s.contains(getString(R.string.json_field_reftran))) {
                try {
                    // Faz o parse do json
                    JSONObject arr = new JSONObject(s);
                    Intent i = new Intent(ConfirmacaoActivity.this,BoletoActivity.class);
                    // Adiciona informações extras para enviar a outra activity
                    i.putExtra(getString(R.string.json_field_reftran),arr.getString(getString(R.string.json_field_reftran)));
                    i.putExtra(getString(R.string.json_field_cod_barra),arr.getString(getString(R.string.json_field_cod_barra)));
                    i.putExtra(getString(R.string.json_field_data_venc),arr.getString(getString(R.string.json_field_data_venc)));
                    i.putExtra(getString(R.string.json_field_valor),valor_num);
                    // Inicia a activity passando os valores extras
                    startActivity(i);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if(s==null)
                // Erro, sem internet!
                Toast.makeText(getApplicationContext(), R.string.error_internet_fail,Toast.LENGTH_LONG).show();
            // Habilita o botão para ser apertado novamente
            bt.setEnabled(true);
            bt.setClickable(true);
        }
    }
}
