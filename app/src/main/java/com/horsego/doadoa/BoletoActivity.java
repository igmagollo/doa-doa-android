package com.horsego.doadoa;

import android.Manifest;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;


// Classe que extend activity e implementa Requisicao Explícita de permissão para I/O
public class BoletoActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{
    // Strings com nomes bem entendíveis
    private String codigoBarras;
    private String refTran;
    private String dataVencimento;
    private String valor_num;
    // TextView do valor do boleto a ser pago
    private TextView valor;
    // TextView do número do boleto
    private TextView num_boleto;
    // DownloadManager usado para baixar o boleto
    private DownloadManager downloadManager;
    // Teoricamente usado para saber o retorno do boleto, mas não usado
    private long refid;
    // Constante de permissão
    static int REQUEST_WRITE_EXTERNAL_STORAGE =1 ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Pega as informações extras mandadas pela outra activity
        Bundle extras = getIntent().getExtras();
        // Checa se essas informações existem
        if(extras==null)
        {
            // Se não existem, aparece mensagem de erro
            Toast.makeText(getApplicationContext(),"Opa opa, deu erro =D",Toast.LENGTH_LONG).show();
            // E volta para a tela anterior
            // Este comando fecha a atual acitivy
            finish();
        }
        else
        {
            //Se existem coloca as informações nas variáveis
            codigoBarras = extras.getString(getString(R.string.json_field_cod_barra));
            refTran = extras.getString(getString(R.string.json_field_reftran));
            dataVencimento = extras.getString(getString(R.string.json_field_data_venc));
            valor_num = extras.getString(getString(R.string.json_field_valor));
        }
        // Seleciona o visual a ser utilizado
        setContentView(R.layout.boleto_handler);
        // Linka visual com código
        valor = findViewById(R.id.valor_fina);
        // Coloca o valor do boleto no TextView
        // TODO: Colocar formatado como currency
        valor.setText(String.format("R$ %s", valor_num));
        // Linka visual com código
        num_boleto = findViewById(R.id.num_boleto);
        // Coloca o número do boleto na Textview
        num_boleto.setText(codigoBarras);
        // Instancia um novo download manager
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        // Registra um listener que checa se o download foi completado
        registerReceiver(onComplete,new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }
    // Listener que checa se o download foi completado
    final BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {
            // Recebe o código de sucesso/erro
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            // Cria uma notificação avisando que o download foi realizado com sucesso
            //TODO: OnClick da notificação
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(BoletoActivity.this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("Doa-Doa")
                            .setContentText("Boleto baixado =D");
            //Exibe a notificação
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(455, mBuilder.build());
            // Ainda não implementado, abrir o boleto
            // abrirBoleto();
        }
    };
    // Se fechar a acitivity, cancelar a notificação
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);

    }
    // Vê se a permissão foi concedida
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    baixarBoleto();
                }
                break;

            default:
                break;
        }
    }
    // Função que copia o conteúdo para o clipboard
    public void copiarParaClipboard(View view)
    {
        // Pega o serviço de clipboard do sistema
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        // Coloca uma 'label' pro novo clip e com o conteúdo do código de barras
        ClipData clip = ClipData.newPlainText("Boleto", codigoBarras);
        // Sobrepõe o que existia antes
        clipboard.setPrimaryClip(clip);
    }
    // Requisita a função explicitamente
    public void pegarPermissoes(View view)
    {
        // Checa se já foi requisitado antes
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        // Se não requisita
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_WRITE_EXTERNAL_STORAGE);
        } else {
        // Se sim, só baixa o boleto
            baixarBoleto();
        }
    }
    // WIP
    public void abrirBoleto()
    {
        String nomearq = codigoBarras.replace(".","");
        nomearq = nomearq.replace(" ","");
        nomearq += ".pdf";
        File file = new File(Environment.DIRECTORY_DOWNLOADS, "/DoaDoaApp/"  + "/" +nomearq);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), getString(R.string.utils_application_pdf));
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }
    // Baixa o boleto
    public void baixarBoleto()
    {
        // Cria um nome de arquivo baseado no número do código de barras
        // Não é o melhor modo possível
        String nomearq = codigoBarras.replace(".","");
        nomearq = nomearq.replace(" ","");
        nomearq += ".pdf";
        // Download url
        Uri Download_Uri = Uri.parse(getString(R.string.server_name)+":"+getString(R.string.server_port)+getString(R.string.api_boleto_get)+"?refTran="+refTran);
        // Usa o downloadmanager do android pra baixar
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        // Manda baixar por wifi e 3g
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        // Por roaming não pode
        request.setAllowedOverRoaming(false);
        // Título pro download manager fazer a notificação
        request.setTitle(getString(R.string.download_manager_boleto) +nomearq);
        // Descrição pro download manager fazer a notificação
        request.setDescription("Baixando " + nomearq);
        // Vísivel no app 'Downloads'
        request.setVisibleInDownloadsUi(true);
        // Destino do arquivo .pdf
        // TODO:Precisamos arrumar isso...
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/DoaDoaApp/"  + "/" +nomearq);
        // Faz o request de download
        refid = downloadManager.enqueue(request);
    }
}