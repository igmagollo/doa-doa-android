package com.horsego.doadoa;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UnusedAcitivityDoar extends Activity{
    public String resposta;
    private Button bt_donation;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_doacao);
        bt_donation = findViewById(R.id.button_donate);
    }

    public void clickMakeDonation(View view) {
        new GetDataTask().execute();
        bt_donation.setEnabled(false);
    }

    private class GetDataTask extends AsyncTask<Void, Void, String> {
        private OkHttpClient client;
        private String url,nome, logradouro,numero,bairro,localidade,uf,cep,cpf_cnpj,valor;
        @Override
        protected void onPreExecute() {
            client =  new OkHttpClient();
            url = "http://www.saocarlos.sp.gov.br/fumcad/gerar.php";
            nome = ((EditText) findViewById(R.id.form_name)).getText().toString();
            logradouro = ((EditText) findViewById(R.id.form_address)).getText().toString();
            numero =((EditText) findViewById(R.id.form_num_end)).getText().toString();
            bairro =((EditText) findViewById(R.id.form_bairro)).getText().toString();
            localidade = ((EditText) findViewById(R.id.form_cidade)).getText().toString();
            uf = ((EditText) findViewById(R.id.form_uf)).getText().toString();
            cep = ((EditText) findViewById(R.id.form_cep)).getText().toString();
            cpf_cnpj = ((EditText) findViewById(R.id.form_doc)).getText().toString();
            valor =  ((EditText) findViewById(R.id.form_value)).getText().toString();
        }
        @Override
        protected String doInBackground(Void... params) {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("nome", nome)
                    .addFormDataPart("logradouro", logradouro)
                    .addFormDataPart("numero", numero)
                    .addFormDataPart("complemento", "someValue")
                    .addFormDataPart("bairro", bairro)
                    .addFormDataPart("localidade", localidade)
                    .addFormDataPart("uf", uf)
                    .addFormDataPart("cep", cep)
                    .addFormDataPart("cpf_cnpj", cpf_cnpj)
                    .addFormDataPart("valor", valor)
                    .addFormDataPart("dest", "2")
                    .addFormDataPart("tipo_taxa", "Doa%C3%A7%C3%A3o+-+FUMCAD")
                    .build();


            Request request = new Request.Builder()
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .url(url)
                    .post(requestBody)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
                // Do something with the response.
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s!=null)
            {
                resposta = s.toString();
                if(resposta.contains("refTran"))
                    new GetDataTask2().execute();
            }
            bt_donation.setEnabled(true);

        }
    }
    private class GetDataTask2 extends AsyncTask<String, String, String> {
        private OkHttpClient client;
        private String url,nome, endereco,cidade,uf,localidade,cep,cpfCnpj,valor;
        private Document doc;
        private String idConv,refTran,dtVenc;
        @Override
        protected void onPreExecute() {
            client =  new OkHttpClient();
            url = "https://mpag.bb.com.br/site/mpag/";
            nome = ((EditText) findViewById(R.id.form_name)).getText().toString();
            endereco = ((EditText) findViewById(R.id.form_address)).getText().toString();
            cidade =((EditText) findViewById(R.id.form_num_end)).getText().toString();
            localidade = ((EditText) findViewById(R.id.form_cidade)).getText().toString();
            uf = ((EditText) findViewById(R.id.form_uf)).getText().toString();
            cep = ((EditText) findViewById(R.id.form_cep)).getText().toString();
            cpfCnpj = ((EditText) findViewById(R.id.form_doc)).getText().toString();
            valor =  ((EditText) findViewById(R.id.form_value)).getText().toString();
            endereco = endereco + " "+((EditText)findViewById(R.id.form_num_end));
            doc = Jsoup.parse(resposta);
            Elements ls = doc.select("input");
            idConv = ls.attr("value");
            Log.v("idConv:",idConv);
            ls = ls.next();
            refTran = ls.attr("value");
            refTran = refTran.replace(": "," ");
            refTran = refTran.trim();
            Toast.makeText(getApplicationContext(),refTran,Toast.LENGTH_LONG).show();
            Log.v("refTran:",refTran);
            ls = ls.next().next();
            dtVenc = ls.attr("value");
            Log.v("dtVenc:",dtVenc);
        }

        @Override
        protected String doInBackground(String ... params) {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("idConv", idConv)
                    .addFormDataPart("refTran", String.valueOf(refTran))
                    .addFormDataPart("valor", valor)
                    .addFormDataPart("dtVenc", dtVenc)
                    .addFormDataPart("tpPagamento", "2")
                    .addFormDataPart("cpfCnpj", cpfCnpj)
                    .addFormDataPart("indicadorPessoa", cep)
                    .addFormDataPart("tpDuplicata", "")
                    .addFormDataPart("urlRetorno", "http://www.saocarlos.sp.gov.br/")
                    .addFormDataPart("nome", nome)
                    .addFormDataPart("endereco", endereco)
                    .addFormDataPart("cidade", cidade)
                    .addFormDataPart("uf", uf)
                    .addFormDataPart("cep", cep)
                    .addFormDataPart("msgLoja", "REFERENTE A DOACAO - FUMCAD. <br>DESTINADO A ENTIDADE ACORDE - ASSOCIACAO DE CAPACITACAO, ORIENTACAO E DESENVOLVIMENTO DO EXCEPCIONAL. <br>NAO RECEBER APOS O VENCIMENTO.")
                    .build();

            Request request = new Request.Builder()
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .url(url)
                    .post(requestBody)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();
                // Do something with the response.
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s!=null)
                Log.v("Resposta",s.toString());
            bt_donation.setEnabled(true);
        }
    }
}
